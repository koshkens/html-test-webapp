import React, { useContext } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from './Components/Navbar/navbar.jsx'
import { webAppContext } from './telegram/telegram.jsx'

function App() {
  const app = useContext(webAppContext)
  let user = app?.initDataUnsafe?.user?.username

  console.log(app)

  return (
    <>
      <div className="App">
        <Navbar name={user ?? 'user_not_found'} />
      </div>
      <div>
        {app.version ? (
          <div className="layout">
            <p>initDataUnsafe</p>
            <code>{JSON.stringify(app.initDataUnsafe, null, 2)}</code>
          </div>
        ) : (
          'loading'
        )}
      </div>
    </>
  )
}

export default App
