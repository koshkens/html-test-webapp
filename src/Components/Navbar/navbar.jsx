import React from 'react'
import './navbar.scss'

export default function Navbar({ name }) {
  return (
    <div className="navbar">
      <div className="container">
        <div className="navbar__main">
          Hello {name}, welcome to test web-app SHOPOBOT
        </div>
      </div>
    </div>
  )
}
